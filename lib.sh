#!/bin/bash

log_environment () {
	pwd
	nproc
	gcc --version
}

prepare_manifest_files () {
	pushd source || exit 1
	local -r description=$(git describe --tags --always)
	popd || exit 1
	for app_id in $1 ; do
		mkdir -p "$app_id/dist"
		echo "$description" > "$app_id/manifest"
		echo "$description" > "$app_id/dist/README.lux.txt"
	done
}

list_dist () {
	{
		find dist -type f
		find dist -type l
	} | sort
}
