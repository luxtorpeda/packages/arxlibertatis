#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

# build Boost
#
readonly boostlocation="$PWD/boost"
pushd "boost"
./bootstrap.sh
./b2 headers
popd

# build Arx Libertatis
#
pushd "source"
mkdir build
cd build || exit 1
ln -s ../../data arx-libertatis-data
cmake \
	-DBoost_DEBUG=1 \
	-DGLM_INCLUDE_DIR=../../glm \
	-DBOOST_ROOT="$boostlocation" \
	-DCMAKE_INSTALL_PREFIX=../../tmp \
	..
make -j "$(nproc)"
make install
popd

cp -rfv tmp/bin/* "1700/dist/"
cp -rfv tmp/lib/* "1700/dist/"
cp -rfv tmp/share/games/arx/* "1700/dist/"
mv 1700/dist/arx 1700/dist/arx-bin
cp -rfv arx-launcher.sh 1700/dist/arx
